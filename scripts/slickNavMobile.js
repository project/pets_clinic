(($, Drupal) => {
  Drupal.behaviors.mobileMenu = {    
    attach: function (context, settings) {
      const slickNav = '.main-menu ul.nav';
      const slicknavMobile = '.mobile-nav';
      $(slickNav).slicknav({
          prependTo: slicknavMobile,
          duration: 300,
          closeOnClick:true,
      });
    }
  };
})(jQuery, Drupal);
