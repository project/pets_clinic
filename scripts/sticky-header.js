((Drupal) => {
  Drupal.behaviors.stickyHeader = {
    attach() {
      document.addEventListener('scroll', function() {
        const headerInner = document.querySelector('.header__main');
        const addHeaderInncerClass = headerInner.classList;
        const headerTopPosition = headerInner.offsetTop;

        if (window.scrollY > headerTopPosition) {
          addHeaderInncerClass.add('sticky');
        } else {
          addHeaderInncerClass.remove('sticky');
        }
      });
    },
  };
})(Drupal);
