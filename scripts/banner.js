((Drupal, once, Splide) => {
  Drupal.behaviors.main_slider = {
    attach(context) {
      once('banner', '.banner', context).forEach(
        (element) => {
          const slide = element.querySelector(
            '[data-drupal-selector="banner"]',
          );
          const splide = new Splide(slide, {
            rewind: true,
            autoplay: false,
            perPage: 1,
            flickMaxPages: 1,
            pagination: false,
            arrows: true,
            classes: {
              arrows: 'container m-auto start-0 end-0 position-absolute top-50',
              page: 'splide__pagination__page testimonial__bullet',
            },
          });

          splide.mount();
        },
      );
    },
  };
})(Drupal, once, window.Splide);
