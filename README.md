# Pets Clinic

The Pets Clinic is a quick starter theme for any type of project.

Pets Clinic is a modern and responsive Drupal theme designed for 
Pet animal organizations. The Pets Clinic theme design is simple
and can easily be modified according to any type of starter project.

The Pets Clinic theme is developed in SASS, Bootstrap 5, Gulp, and
Slicknav JS for mobile menu, fontawesome icons, Splidejs for slider.

**Features**
- Fully responsive
- Theme configuration options are as below
  - Slide
  - Social media links
  - Copyright Message
  - Address, Mobile Number, Email ID


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal Theme. For further
information, see
[Installing Drupal Themes](https://www.drupal.org/docs/extending-drupal/themes/installing-themes).


## Configuration

1. After installation navigate to theme folder and run npm install command.
1. Now navigate to Appearience page from admin and find theme 'Pet Clinic'.
1. Click on 'Enable and Set Default' theme.
1. Now navigate to home page and refresh or clear cache.

[Congiguration theme](https://www.drupal.org/docs/user_guide/en/extend-theme-install.html)


## Maintainers

- Ravi Kant Kumawat - [ravi-kant](https://www.drupal.org/u/ravi-kant)
