const { src, dest, watch, series, parallel } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
var paths = {
 sassSrc: 'scss/**/*.scss',
 sassDest: 'dist/css/',
 jsSrc: 'scripts/**/*.js',
 jsDest: 'dist/js/',
}

function styles() {
 return src(paths.sassSrc)
   .pipe(sassGlob())
   .pipe(sourcemaps.init())
   .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
   .pipe(sourcemaps.write('./'))
   .pipe(dest(paths.sassDest));
}

function scripts() {
 return src(paths.jsSrc)
   .pipe(sourcemaps.init())
   .pipe(uglify())
   //.pipe(concat('scripts.js'))
   .pipe(sourcemaps.write('./'))
   .pipe(dest(paths.jsDest));
}

exports.styles = styles;
exports.scripts = scripts;
exports.watch = function () {
 watch(paths.sassSrc, styles);
 watch(paths.jsSrc, scripts);
};
exports.default = parallel(styles, scripts);
